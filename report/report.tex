%% 
%% Created in 2018 by Martin Slapak
%%
%% Based on file for NRP report LaTeX class by Vit Zyka (2008)
%%
%% Compilation:
%% >pdflatex report
%% >bibtex report
%% >pdflatex report
%% >pdflatex report

\documentclass[english]{mvi-report}

\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{caption}
\usepackage{subcaption}

\title{Pattern recognition on wafer bin maps}

\author{Matěj Latka}
\affiliation{CTU FIT}
\email{latkamat@fit.cvut.cz}

\def\file#1{{\tt#1}}

\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Assignment}
The task is to create an augmented dataset of defects on wafer bin maps and train a map size independent CNN classifier to recognize them. A generative adversarial network (GAN) was used for augmentation and a CNN was used for classification.
\noindent
The code is available on FIT GitLab: https://gitlab.fit.cvut.cz/latkamat/mvi-sp or on Kaggle (notebook with data augmentation: \linebreak https://www.kaggle.com/matjlatka/ptrn-rec-on-w-bin-maps-generative-augmentation, notebook with classification: https://www.kaggle.com/matjlatka/ptrn-rec-on-w-bin-maps-custom-classifier).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data}
\subsection{Description}
The data was provided by the \textit{\href{https://www.onsemi.com}{onsemi}} company. Data consists of 14473 CSV files, each representing a wafer bin map. Value -1 represents \textit{background}, value 0 represents \textit{good bin}, and value greater than zero represents \textit{bad bin}. Dataset is divided into multiple categories: \textit{clear} (9999 samples), \textit{field} (368 samples), \textit{mask} (2897 samples), \textit{probe} (1080 samples), and \textit{scratch} (129 samples).

\subsection{Interpretation}
To be able to distinguish one data class from another, a colour map was created for better visualization. On this colormap, \textit{background} is violet, \textit{good bins} are green and \textit{bad bins} vary from yellow to red according to the pixel value. Original dataset (example on Figure~\ref{fig:scratch-bg}) has been named \textit{dataset with distinctive background}.\\
\\
Also, \textit{dataset without distinctive background} (Figure~\ref{fig:scratch-no-bg}) was created. Images in this dataset do not have \textit{background}, its value of -1 has been replaced by value 0 of \textit{good bins}. This means that the dataset has been stripped off the information about the wafer bin map shape and basic structure, but this information is the same for all images so there was no significant information loss as all information about defects has been kept. Models working with such dataset will be able to focus entirely on defective patterns.\\
\\
All images regardless of their interpretation were scaled onto interval $\langle 0; 1\rangle$ before being used by any model for training or evaluation.
\begin{figure}[h]
	\centering
	\begin{subfigure}{.4\linewidth}
		\centering
		\includegraphics[width=\linewidth]{img/scratch-bg.png}
		\subcaption{Colormap applied on wafer bin map with distinctive background.}
		\label{fig:scratch-bg}
	\end{subfigure}
	\begin{subfigure}{.4\linewidth}
		\centering
		\includegraphics[width=\linewidth]{img/scratch-no-bg.png}
		\subcaption{Colormap applied on wafer bin map without distinctive background.}
		\label{fig:scratch-no-bg}
	\end{subfigure}
\end{figure}
\subsection{Balancing}
As the dataset had been heavily imbalanced, it has been balanced using random under-sampling, random over-sampling and data augmentation. Not all augmentation techniques could be used, only those respecting characteristics of the wafer bin map. \textit{Dataset with distinctive background} has been rather strict in that matter because of the visible wafer bin map outline. Thus, only horizontal and vertical flipping could be used. \textit{Dataset without distinctive background} permitted use of other techniques as well, such as rotating and scaling (focusing and unfocusing along each axis) \cite{augmetnation-techniques}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Methods}
Generative adversarial networks (GAN) are commonly used for data augmentation \cite{augmetnation-techniques}. This architecture consists of two neural networks -- a generator and a discriminator. The generator starts with input image (random noise in our case) and tries to produce a required output. Meanwhile, the discriminator is being supplied with both real and generated images and decides which sample is real and which is generated. The goal is that the generator is so good that the discriminator is not able to tell generated and real images apart. For our experiments, Deep Convolutional Generative Adversarial Network (\nolinebreak{DAGAN}) \cite{radford2016unsupervised} was used.\\
\\
Convolutional neural networks are widely used for image classification. There are many different architectures using many types of layers, the most important being convolutional layers for feature extraction, pooling layers for dimensionality reduction and fully connected layers for classification.

\section{Experiments}
\subsection{Generative augmentation}
Balanced datasets used for generative augmentation consisted of different number of images. As augmentation techniques for dataset with distinctive background were limited, not enough images from the most heavily under-sampled class \textit{scratch} could be generated and over-sampling it too much afterwards wouldn't be much useful. Final \textit{dataset with distinctive background} consisted of 2000 samples from each defective class (that means all classes except \textit{clear}), \textit{dataset without distinctive background} consisted of 4000 samples from each defective class.\\
\\
Three instances of the GAN (that means six neural networks -- three generators and three discriminators) were created. The first GAN was trained on defective classes with background, the second GAN on defective classes without background and the third GAN on the data without background as well, but only on the most heavily under-sampled defective class alone -- \textit{scratch}. GANs were trained on 100 epochs each, after which they were able to produce data shown on Figures \ref{fig:gan-bg-output}, \ref{fig:gan-no-bg-output} and \ref{fig:gan-no-bg-scratch-output}.
\begin{figure}[h]
	\centering
	\begin{subfigure}{.4\linewidth}
		\centering
		\includegraphics[width=\linewidth]{img/gan-output-bg-original.png}
		\subcaption{Original output}
	\end{subfigure}
	\begin{subfigure}{.4\linewidth}
		\centering
		\includegraphics[width=\linewidth]{img/gan-output-bg-color.png}
		\subcaption{Colormap applied}
	\end{subfigure}
	\caption{GAN output on dataset with background}
	\label{fig:gan-bg-output}
\end{figure}

\begin{figure}[h]
	\centering
	\begin{subfigure}{.4\linewidth}
		\centering
		\includegraphics[width=\linewidth]{img/gan-output-no-bg-original.png}
		\subcaption{Original output}
	\end{subfigure}
	\begin{subfigure}{.4\linewidth}
		\centering
		\includegraphics[width=\linewidth]{img/gan-output-no-bg-color.png}
		\subcaption{Colormap applied}
	\end{subfigure}
	\caption{GAN output on dataset without background}
	\label{fig:gan-no-bg-output}
\end{figure}

\begin{figure}[h]
	\centering
	\begin{subfigure}{.4\linewidth}
		\centering
		\includegraphics[width=\linewidth]{img/gan-output-no-bg-scratch-original.png}
		\subcaption{Original output}
	\end{subfigure}
	\begin{subfigure}{.4\linewidth}
		\centering
		\includegraphics[width=\linewidth]{img/gan-output-no-bg-scratch-color.png}
		\subcaption{Colormap applied}
	\end{subfigure}
	\caption{GAN output on \textit{scratch} without background}
	\label{fig:gan-no-bg-scratch-output}
\end{figure}

\subsection{Classification with CNN}
For classification, Convolutional Neural Network (CNN) was used. CNNs had been successfully used along with other methods for pattern recognition on wafer bin maps before \cite{pattern-recognition-wafer-bin-maps} \cite{pattern-recognition-wafer-bin-maps-2}. An architecture accepting images of wafer bin maps slightly enlarged and squared to 128 $\times$ 128 pixels was developed. The output layer consists of 5 neurons with softmax function predicting probabilities of whether the image belongs to the respective class.\\
\\
Before training, datasets were split into train and test datasets in proportion 7:3. The split had been made along each class separately so that the test dataset reflected how imbalanced the data originally were. After that, the train dataset has been balanced, destroying class-wise 7:3 proportion but enabling better training for minority classes. Composition of final datasets is presented in Tables \ref{tab:classification-dataset-bg} and \ref{tab:classification-dataset-no-bg}.\\
\\
Separate CNN for each dataset was created. Each network was trained on a balanced dataset with all classes. During training, the EarlyStoppping callback with the patience of 30 epochs has been used. The stopping criterion was validation accuracy.

\begin{table}[h]\centering
	\begin{tabular}{|l||r|r|r|}
		\hline
		Class & Train imbal. & Train balanced & Test \\
		\hline
		\hline
		\textit{clear} & 6999 & 1500 & 3000 \\
		\hline
		\textit{field} & 257 & 1500 & 111 \\
		\hline
		\textit{mask} & 2027 & 1500 & 870 \\
		\hline
		\textit{probe} & 756 & 1481 & 324 \\
		\hline
		\textit{scratch} & 90 & 1500 & 39 \\
		\hline
		\hline
		\textbf{total} & \textbf{10129} & \textbf{7481} & \textbf{4344}\\
		\hline
	\end{tabular}
	\caption{Classification dataset with background}
	\label{tab:classification-dataset-bg}
\end{table}

\begin{table}[h]\centering
	\begin{tabular}{|l||r|r|r|}
		\hline
		Class & Train imbal. & Train balanced & Test \\
		\hline
		\hline
		\textit{clear} & 6999 & 3000 & 3000 \\
		\hline
		\textit{field} & 257 & 3004 & 111 \\
		\hline
		\textit{mask} & 2027 & 2965 & 870 \\
		\hline
		\textit{probe} & 756 & 2902 & 324 \\
		\hline
		\textit{scratch} & 90 & 3000 & 39 \\
		\hline
		\hline
		\textbf{total} & \textbf{10129} & \textbf{14871} & \textbf{4344}\\
		\hline
	\end{tabular}
	\caption{Classification dataset without background}
	\label{tab:classification-dataset-no-bg}
\end{table}
\subsection{Transfer learning - classification with pre-trained CNN}
The intention was to use a pre-trained classifier for wafer bin map classification to compare its performance with the CNN architecture used in previous subsection. Criteria for the best classifier were as follows: precision and performance -- the higher, the better; complexity and size -- preferably not too big; expected data format -- as similar to the format used for training custom CNN as possible. Finally, Xception model \cite{xception} implementation available in Keras was selected, for its reasonable performance and complexity and similar input data format -- shape $(299, 299, 3)$ with input pixels scaled between $-1$ and $1$ (CNN classifier from the previous subsection used shape of $(128, 128, 1)$ and input pixels scaled between $0$ and $1$). However, projecting datasets from 2D to 3D made them too big to store in RAM even after decreasing datasets size by $75\,\%$ and optimizing data type.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results}
As we can see in Figure \ref{fig:gan-bg-output}, there are two vertical lines in the middle of the wafer bin map produced by the generative network trained on \textit{dataset with distinctive background}. Most points on those lines are considered as defective bins, some as background. This is probably a combination of a structural defect developed in GAN and influence of certain defective bin shapes, such as \textit{probe}.\\
\\
GAN trained on \textit{dataset without distinctive background}'s defective classes learned the \textit{field}, \textit{probe} and \textit{scratch} defect, but was rather confused when putting them in one wafer bin map (Figure \ref{fig:gan-no-bg-output}). The most prominent is repeating pattern of \textit{field} defects.\\
\\
When trained on \textit{dataset without distinctive background}'s \textit{scratch} class, GAN learned shape of the defect, but when upon generation placed multiple scratches one over another at several parts of the map (Figure \ref{fig:gan-no-bg-scratch-output}).\\
\\
Overall, CNN classifiers trained on datasets with and without background achieved very high validation accuracy of $95.21\,\%$ and $93.76\,\%$, respectively. Validation accuracy has also been the criterion to find the epoch after which the model performed the best. Apart from accuracy, also loss has been monitored and F1-score and F2-score were computed for all classes. Metric curves can be seen on Figures \ref{fig:cnn-bg-curves} and \ref{fig:cnn-no-bg-curves}. The best performance for each model can be viewed on Tables \ref{tab:cnn-bg-table} and \ref{tab:cnn-no-bg-table}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{img/cnn-bg-curves.png}
	\caption{Metric curves for CNN classifying images with background.}
	\label{fig:cnn-bg-curves}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{img/cnn-no-bg-curves.png}
	\caption{Metric curves for CNN classifying images without background.}
	\label{fig:cnn-no-bg-curves}
\end{figure}

\begin{table}[h]\centering
	\begin{tabular}{|l||c|c|}
		\hline
		Metrics & Train & Validation \\
		\hline
		\hline
		Accuracy & 0.9598 & 0.9521 \\
		\hline
		Loss & 0.1358 & 0.1659 \\
		\hline
		\hline
		\multicolumn{3}{|l|}{F1-score} \\
		\hline
		\hline
		F1 - clear & 0.9768 & 0.9858 \\
		\hline
		F1 - field & 0.9873 & 0.9493 \\
		\hline
		F1 - mask & 0.9570 & 0.9403  \\
		\hline
		F1 - probe & 0.9145 & 0.7304  \\
		\hline
		F1 - scratch & 0.9618 & 0.2222  \\
		\hline
		\hline
		\multicolumn{3}{|l|}{F2-score} \\
		\hline
		\hline
		F2 - clear & 0.9791 & 0.9861 \\
		\hline
		F2 - field & 0.9869 & 0.9364 \\
		\hline
		F2 - mask & 0.9651 & 0.9519  \\
		\hline
		F2 - probe & 0.9033 & 0.7236 \\
		\hline
		F2 - scratch & 0.9631 & 0.1754 \\
		\hline
	\end{tabular}
	\caption{Best performance of CNN classifying images with background.}
	\label{tab:cnn-bg-table}
\end{table}

\begin{table}[h]\centering
	\begin{tabular}{|l||c|c|}
		\hline
		Metrics & Train & Validation \\
		\hline
		\hline
		Accuracy & 0.9899 & 0.9376 \\
		\hline
		Loss & 0.0218 & 0.5059 \\
		\hline
		\hline
		\multicolumn{3}{|l|}{F1-score} \\
		\hline
		\hline
		F1 - clear & 0.9990 & 0.9799 \\
		\hline
		F1 - field & 0.9970 & 0.8908 \\
		\hline
		F1 - mask & 0.9796 & 0.9195  \\
		\hline
		F1 - probe & 0.9789 & 0.6831  \\
		\hline
		F1 - scratch & 0.9945 & 0.2500  \\
		\hline
		\hline
		\multicolumn{3}{|l|}{F2-score} \\
		\hline
		\hline
		F2 - clear & 0.9992 & 0.9772 \\
		\hline
		F2 - field & 0.9966 & 0.9282 \\
		\hline
		F2 - mask & 0.9797 & 0.9271  \\
		\hline
		F2 - probe & 0.9779 & 0.6843 \\
		\hline
		F2 - scratch & 0.9956 & 0.2210  \\
		\hline
	\end{tabular}
	\caption{Best performance of CNN classifying images with background.}
	\label{tab:cnn-no-bg-table}
\end{table}
\noindent
Both F1-score and F2-score are satisfyingly high, worse values were achieved only for the class \textit{probe} and \textit{scratch}. Differences between F1 and F2 scores are not big enough to determine, whether it is affected more by precision or by recall. However, cause for \textit{probe} may be its interchangeability with other classes, cause for \textit{scratch} may be that data augmentation was not able to create unique enough samples for the classifier to securely determine \textit{scratches} from other classes and also a heavy use of over-sampling on this particular class.\\
\\
It is hard to tell which dataset is better, results are comparable, judging by validation loss curves, CNN trained on \textit{dataset without distinctive background} is likely to overfit more quickly.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion and future prospects}
Two interpretations of the wafer bin map dataset were created -- one with standard \textit{background}, other with \textit{background} transformed to \textit{good bins}.\\
\\
Generative adversarial networks were used for generating defective wafer bin maps as well as for generating one particular class of defects, however, the output was not good enough to be incorporated into the dataset.\\
\\
Convolutional neural network was used to classify wafer bin maps. The train dataset had been balanced before, using over-sampling, under-sampling and traditional methods of data augmentation with respect to the domain specificity. Results are very good for three classes, good for one class and slightly worse for another, this is due to the very rare occurrence of this class in the imbalanced dataset. All classifiers are map size-independent as images are resized before both training and evaluation. One traditional data augmentation technique used for data balancing (scaling) also prepares the classifier for differently sized wafer bin maps and defects.\\
\\
This task may be developed further, author would suggest to run pre-trained classifier in more suitable environment with more RAM available and compare its performance with the already used CNN classifier. Also, the classifier's performance on the \textit{scratch} class may be enhanced. We have seen that both classifiers tend to overfitting so it may be useful to reduce number of samples in train dataset so that it is not necessary to over-sample augmented \textit{scratches} so much.\\
\\
I hope that the \textit{onsemi} company will find this semestral project useful.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Bibliography
\bibliography{reference}

\end{document}
