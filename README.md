# mvi-sp
# Pattern recognition on wafer bin maps 
## Assignment
Create an augmented dataset of defects on wafer bin maps and train a map size independent CNN classifier to recognize them.

## Milestone report
- [milestone.pdf](https://gitlab.fit.cvut.cz/latkamat/mvi-sp/blob/master/milestone.pdf) in this repository

## Final submission
Report:
- [report.pdf](https://gitlab.fit.cvut.cz/latkamat/mvi-sp/blob/master/report.pdf) in this repository

Source code in this repository
- [notebook with generative augmentation](https://gitlab.fit.cvut.cz/latkamat/mvi-sp/blob/master/ptrn-rec-on-w-bin-maps-generative-augmentation.ipynb)
- [notebook with CNN classifier](https://gitlab.fit.cvut.cz/latkamat/mvi-sp/blob/master/ptrn-rec-on-w-bin-maps-custom-classifier.ipynb)

Identical source code and dataset on Kaggle
- [notebook with generative augmentation](https://www.kaggle.com/matjlatka/ptrn-rec-on-w-bin-maps-generative-augmentation)
- [notebook with CNN classifier](https://www.kaggle.com/matjlatka/ptrn-rec-on-w-bin-maps-custom-classifier)
- [wafer bin map dataset](https://www.kaggle.com/matjlatka/wafer-bin-maps)
